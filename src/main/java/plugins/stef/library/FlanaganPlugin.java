package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Flanagan scientific library for Icy.
 * Copyright Dr Michael Thomas Flanagan
 * 
 * @author Stephane Dallongeville
 */
public class FlanaganPlugin extends Plugin implements PluginLibrary
{
    //
}
